import React, { useEffect, useCallback } from 'react'
import debounce from 'lodash.debounce';
import PostItem from './PostItem'
import { postListStyles } from 'styles'

/**
 * Post list component
 */
const PostList = ({ posts, onEnd, done, }) => {
  const debouncedHandleScroll = useCallback(debounce(handleScroll, 250), []);
  useEffect(() => {
    window.addEventListener('scroll', debouncedHandleScroll);
    return () => window.removeEventListener('scroll', debouncedHandleScroll);
  }, []);

  function handleScroll() {
    if (Math.round(window.innerHeight + document.documentElement.scrollTop) !== document.documentElement.offsetHeight) return;
    onEnd();
  }

  return (
    <div className="blog-main">
      {posts.map((post) => (
        <PostItem post={post} key={post.id} />
      ))}
      {done ? <p style={{textAlign: 'center', fontStyle: 'italic'}}>All posts fetched</p>: null}
      <style jsx global>{postListStyles}</style>
    </div>
  )
}

export default PostList
