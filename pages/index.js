import React, { useState, useEffect } from "react";
import Head from "next/head";
import Prismic from "prismic-javascript";
import { RichText } from "prismic-reactjs";

// Project components & functions
import DefaultLayout from "layouts";
import { Header, PostList, SetupRepo } from "components/home";
import { Client } from "utils/prismicHelpers";

/**
 * Homepage component
 */
const Home = ({ doc, initPosts, initPage, pageSize, totalPages }) => {
  if (doc && doc.data) {
 
    const [posts, setPosts] = useState(initPosts);
    const [page, setPage] = useState(initPage);
    useEffect(() => {
      if (page !== initPage && page <= totalPages) {

        const { getPosts } = clientObject;

        console.log('Fetching data for page:', page)
        getPosts(page, pageSize, undefined)
          .then(newPosts => setPosts([...posts, ...newPosts.results]))
      }
    }, [page]);

    const onEnd = () => {
      setPage(page => {
        if (page === totalPages) {
          return page;
        }

        return page  + 1;
      });
    }

    return (
      <DefaultLayout>
        <Head>
          <title>{RichText.asText(doc.data.headline)}</title>
        </Head>
        <Header
          image={doc.data.image}
          headline={doc.data.headline}
          description={doc.data.description}
        />
        <PostList 
          posts={posts} 
          onEnd={onEnd}
          done={page === totalPages}
        />
      </DefaultLayout>
    );
  }

  // Message when repository has not been setup yet
  return <SetupRepo />;
};


const clientObject = (() => {
  const client = Client();
  return {
    client,
    getPosts: (page=1, pageSize=100, params) => client.query(
      Prismic.Predicates.at("document.type", "post"), {
        page,
        pageSize, 
        orderings: "[my.post.date desc]",
        ...(params ? { params } : null),
      },
    )
  }
})();

export async function getStaticProps({ preview = null, previewData = {} }) {

  const { ref } = previewData

  const { client, getPosts } = clientObject

  const doc = await client.getSingle("blog_home", ref ? { ref } : null) || {}

  const posts = await getPosts(1, 2, ref);

  return {
    props: {
      doc,
      preview,
      initPosts: posts ? posts.results : [],
      initPage: 1,
      totalPages: posts['total_pages'],
      pageSize: 2,
    }
  }
}

export default Home;
